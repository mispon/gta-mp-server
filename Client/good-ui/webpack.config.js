'use strict'

const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: 'styles/[name].css'
});

let NODE_ENV;
if (process.env.NODE_ENV) {
    NODE_ENV = process.env.NODE_ENV.replace(/^\s+|\s+$/g, "")
}

module.exports = {
    context: __dirname,
    entry: {
        interface: ['./scripts/interface/main.js', './scripts/interface/timer.js', './scripts/interface/gift.js']
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: 'build/',
        filename: 'scripts/[name].js',
        library: '[name]',
    },
    devServer: {
        inline: true,
        hot: true,
        contentBase: './'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: [/node_modules/],
            loader: 'babel-loader',
            query: {
                presets: ['es2015']
            }
        }, {
            test: /\.scss$/,
            exclude: [/node_modules/],
            use: ['css-hot-loader'].concat(extractSass.extract({
                use: [{
                    loader: "css-loader",
                    options: { minimize: true }
                }, {
                    loader: "sass-loader"
                }],
                fallback: "style-loader"
            }))
        }]
    },
    watch: NODE_ENV == 'dev',
    plugins: [
        new webpack.EnvironmentPlugin(['NODE_ENV']),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        }),
        extractSass
    ]
}